<?php
require_once('animals.php');
require_once('ape.php');
require_once('frog.php');
$sheep = new Animal("shaun");
echo "<h1>Release 0</h1>";
echo $sheep->get_name(); // "shaun"
echo "<br>Jumlah Kakinya : ".$sheep->get_legs(); // 2
echo "<br>Berdarah dingin ? ".$sheep->get_cold_blooded(); // false

echo "<h1>Release 1</h1>";
$sungokong = new Ape("kera sakti");
echo "<h3>".$sungokong->get_name()."</h3>";
echo $sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
echo "<h3>".$kodok->get_name()."</h3>";
echo $kodok->jump() ; 
echo "<br>Jumlah kakinya : ".$kodok->legs ;
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded()
$number[0]=1;
$number[]=2;
$number[]=3;
foreach($number as $x){
    echo $x;
}
?>